import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="container p-0 d-flex justify-content-center">
        <navbar-component></navbar-component>
    </div>
    <router-outlet></router-outlet>
    <app-footer></app-footer>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'NortalBlog';
}
