export interface IPost {
    id: number;
    postMainTitle: string;
    postSubTitle: string;
    postLead: string;
    postBodytext: string;
    postImage: string
}

export class Post implements IPost {
    constructor(
        public id: number,
        public postMainTitle: string,
        public postSubTitle: string,
        public postLead: string,
        public postBodytext: string,
        public postImage: string
    ) {}
}