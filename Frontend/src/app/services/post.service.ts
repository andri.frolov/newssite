import { Injectable } from '@angular/core';
import { IPost, Post } from '../models/post';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class PostService {
    private baseUrl = 'http://localhost:8080/posts';
    posts: IPost[] = [];

    constructor(private http: HttpClient) {}

    getPosts(): Observable<IPost[]> {
        return this.http.get<IPost[]>(`${this.baseUrl}/getPosts`).
            pipe(catchError(this.handleError));
    }

    getSinglePostById(incomingId: number): Observable<IPost> {
          const url = `${this.baseUrl}/getPosts/${incomingId}`;
          return this.http.get<IPost>(url)
    }

    deletePost(id: number): Observable<{}> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const url = `${this.baseUrl}/deletePost/${id}`;
        return this.http.delete<Post>(url, { headers: headers });
    }

    updatePost(updatedPost: Post): Observable<Post> {
        const headers = new HttpHeaders({ 
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        });

        const url = `${this.baseUrl}/editPost`;
        return this.http.put<Post>(
            url, updatedPost, { headers: headers})
            .pipe(
                tap(() => console.log('updatePost: ' + updatedPost.id)),
                map(() => updatedPost),
                catchError(this.handleError)
            );
    }

    createPost(post: Post): Observable<Post> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        })

        const url = `${this.baseUrl}/editPost`;
        return this.http.post<Post>(
            url, post, {headers: headers})
    }

    private handleError(err: HttpErrorResponse) {
        let errorMessage = '';
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return throwError(errorMessage);
    }
}