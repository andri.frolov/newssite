import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavBar } from 'src/app/components/navbar/navbar.component';
import { Footer } from 'src/app/components/footer/footer.component';

@NgModule({
  declarations: [
    NavBar,
    Footer
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    NavBar,
    Footer,
    CommonModule,
    RouterModule
  ]
})
export class SharedModule { }
