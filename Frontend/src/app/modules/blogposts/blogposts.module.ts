import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SinglePostPage } from 'src/app/components/singlepost-view/singlepost-view.component';
import { PostPage } from 'src/app/components/new-post/new-post.component';
import { PostEdit } from 'src/app/components/post-edit.component/post-edit.component';
import { LandingPage } from 'src/app/components/landing-page/landing-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LandingPageGuard } from 'src/app/guards/landing-page.guard';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    LandingPage,
    SinglePostPage,
    PostPage,
    PostEdit
  ],
  imports: [
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: 'new-post', component: PostPage },
      { path: 'edit-post/:id', component: PostEdit },
      { path: 'welcome', component: LandingPage },
      { path: 'welcome/:id',
        canActivate: [LandingPageGuard],
        component: SinglePostPage
      },
    ]),
    SharedModule
  ],
  exports: [
    SharedModule
  ]
})
export class PostsModule { }
