import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Post } from '../../models/post';
import { PostService } from 'src/app/services/post.service';
import { Router } from '@angular/router';

@Component({
    selector: 'postPage',
    templateUrl: './new-post.component.html',
    styleUrls: ['./new-post.component.scss']

})

export class PostPage implements OnInit {
    postForm: FormGroup;
    post: Post;
    errorMessage: string;
    
    constructor(
        private fb: FormBuilder,
        private postService: PostService,
        private router: Router
    ) { }

    ngOnInit() {
        this.postForm = this.fb.group({
            postMainTitle: ['', [Validators.required, Validators.maxLength(150)]],
            postSubTitle: ['', Validators.maxLength(200)],
            postLead: ['', [Validators.required, Validators.maxLength(300)]],
            postBodytext: ['', [Validators.required, Validators.maxLength(20000)]],
            postImage: ['', [Validators.required, Validators.maxLength(500)]]
        });

        this.postForm.get('postLead').valueChanges.subscribe(
            value => console.log(value)
        )
    }

    populateTestData(): void {
        this.postForm.patchValue({
            postMainTitle: 'EL-i uued lennundusreeglid nõuavad kaitsemaski kandmist kogu reisi vältel',
            postSubTitle: '',
            postLead: 'Euroopa Liidu koroonakriisi aegsed soovitused turvaliseks lendamiseks näevad ette kaitsemaskide kandmist, inimeste vahel võimalusel 1,5-meetrise vahemaa hoidmist ja kontaktivaba teenindust.',
            postBodytext: 'Euroopa Liidu Lennuohutuse Agentuur (EASA) ja Euroopa Haiguste Ennetamise ja Kontrolli Keskus (ECDC) kolmapäeval avaldatud ühisdokumendi kohaselt peavad lennufirmad tegema kõik selleks, et haiged inimesed lennujaama ega lennukisse ei tuleks.',
            postImage: 'https://s.err.ee/photo/crop/2020/05/21/781089he610t28.jpg'
        })
    }

    savePost(): void {
        if (this.postForm.valid) {
            if (this.postForm.dirty) {
                const post = { ...this.post, ...this.postForm.value }

                this.postService.createPost(post)
                    .subscribe({
                        next: () => this.onSaveComplete(),
                        error: err => this.errorMessage = err
                    })
            } else {
                this.onSaveComplete()
            }
        } else {
            this.errorMessage = "Please correct validation errors."
        }
    }
    onSaveComplete(): void {
        this.postForm.reset();
        this.router.navigate(['/welcome'])
    }

    save() {
        console.log(this.postForm);
        console.log('Saved: ' + JSON.stringify(this.postForm));
    }
}