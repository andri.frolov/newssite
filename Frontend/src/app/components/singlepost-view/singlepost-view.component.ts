import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from 'src/app/services/post.service';
import { IPost, Post } from '../../models/post';

@Component({
  selector: 'app-singlepost.view',
  templateUrl: './singlepost-view.component.html',
  styleUrls: ['./singlepost-view.component.scss']
})
export class SinglePostPage implements OnInit {
  post: IPost | undefined;
  errorMessage = '';

  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
    private router: Router) {
    console.log("singlepost-view.componenti id: " + this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit(): void {
    const idFromUrl = +this.route.snapshot.paramMap.get('id');
    if (idFromUrl) {
      this.getSinglePost(idFromUrl);
    }
  }

  getSinglePost(id: number): void {
    this.postService.getSinglePostById(id).subscribe({
      next: (post: Post) => this.post = post,
      error: err => this.errorMessage = err
    })
  }
  
  deleteBlogpost(): void {
    if (this.post.id === 0) {
      this.goBack();
    } else {
      if (confirm(`Kas oled kindel, et soovid kustutada postitust pealkirjaga "${this.post.postMainTitle}"?`)) {
        this.postService.deletePost(this.post.id).subscribe({
          next: () => this.goBack(),
          error: err => this.errorMessage = err
        });
      }
    }
  }

  goBack(): void {
    this.router.navigate(['/welcome'])
  }

}
