import { Component, OnInit } from '@angular/core';
import { IPost } from '../../models/post';
import { PostService } from 'src/app/services/post.service';

@Component({
    selector: 'landing-page',
    templateUrl: './landing-page.component.html',
    styleUrls: ['./landing-page.scss']
})

export class LandingPage implements OnInit {
    posts: IPost[] = [];    
    errorMessage: string;

    constructor(private postService: PostService) {

    }

    ngOnInit(): void {
        this.postService.getPosts().subscribe({
            next: posts => this.posts = posts,
            error: err => this.errorMessage = err
        });
    }
}