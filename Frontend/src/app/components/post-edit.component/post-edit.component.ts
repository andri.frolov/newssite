import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'editPage',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.scss']
})
export class PostEdit implements OnInit {
  postForm: FormGroup;
  errorMessage: string;
  private sub: Subscription;
  post: Post;

  constructor(
    private fb: FormBuilder,
    private postService: PostService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.postForm = this.fb.group({
      postMainTitle: ['', [Validators.required, Validators.maxLength(150)]],
      postSubTitle: ['', Validators.maxLength(200)],
      postLead: ['', [Validators.required, Validators.maxLength(300)]],
      postBodytext: ['', [Validators.required, Validators.maxLength(20000)]],
      postImage: ['', [Validators.required, Validators.maxLength(500)]]
    })

    this.sub = this.route.paramMap.subscribe(
      params => {
        const id = +params.get('id');
        this.getPostDataForForm(id);
      }
    )
  }

  getPostDataForForm(id: number): void {
    this.postService.getSinglePostById(id)
      .subscribe({
        next: (post: Post) => this.fillPostForm(post),
        error: err => this.errorMessage = err
      })
  }

  fillPostForm(post: Post): void {
    this.post = post;
    this.postForm.patchValue({
      postMainTitle: this.post.postMainTitle,
      postSubTitle: this.post.postSubTitle,
      postLead: this.post.postLead,
      postBodytext: this.post.postBodytext,
      postImage: this.post.postImage
    })
  }

  savePost(): void {
    if (this.postForm.valid) {
      if (this.postForm.dirty) {
        const p = {...this.post, ...this.postForm.value}

        this.postService.updatePost(p)
          .subscribe({
            next: () => this.onSaveComplete(),
            error: err => this.errorMessage = err
          });
      } else {
        this.onSaveComplete();
      }
    } else {
      this.errorMessage = 'Please correct the validation errors.';
    }
  }

  onSaveComplete(): void {
    this.postForm.reset();
    let postIdFromRoute = this.route.snapshot.paramMap.get('id');
    this.router.navigate(['/welcome', postIdFromRoute])
  }

}
