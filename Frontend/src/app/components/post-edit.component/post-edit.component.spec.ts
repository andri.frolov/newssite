import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostEdit } from './post-edit.component';

describe('PostEdit', () => {
  let component: PostEdit;
  let fixture: ComponentFixture<PostEdit>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostEdit ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostEdit);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
