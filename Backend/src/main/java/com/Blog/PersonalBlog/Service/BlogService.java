package com.Blog.PersonalBlog.Service;

import com.Blog.PersonalBlog.Model.Post;
import com.Blog.PersonalBlog.Repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlogService
{
    @Autowired
    private PostRepository postRepository;

    public void editPost(Post blogpost)
    {
        if (blogpost.getId() > 0) {
            System.out.println("Updating existing blogpost...");
            postRepository.updateExistingBlogpost(blogpost);
        } else {
            System.out.println("Creating new blogpost...");
            postRepository.addNewBlogpost(blogpost);
        }
    }
}
