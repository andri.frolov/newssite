package com.Blog.PersonalBlog.Repository;

import com.Blog.PersonalBlog.Model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PostRepository
{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Post> fetchAllPosts()
    {
        return jdbcTemplate.query("Select * from posts;",
                (dbRow, sequenceNumber) ->
                {
                    return new Post(
                            dbRow.getInt("id"),
                            dbRow.getString("postMainTitle"),
                            dbRow.getString("postSubTitle"),
                            dbRow.getString("postLead"),
                            dbRow.getString("postBodytext"),
                            dbRow.getString("postImage")
                    );
        });
    }

    public Post fetchSinglePostById(int id)
    {
        List<Post> singlePost = jdbcTemplate.query("Select * from posts where id = ?;",
                new Object[] {id},
                (dbRow, sequenceNumber) ->
                {
                    return new Post(
                            dbRow.getInt("id"),
                            dbRow.getString("postMainTitle"),
                            dbRow.getString("postSubTitle"),
                            dbRow.getString("postLead"),
                            dbRow.getString("postBodytext"),
                            dbRow.getString("postImage")
                    );
                });
        if (singlePost.size() > 0) {
            return singlePost.get(0);
        } else {
            return null;
        }
    }

    public void addNewBlogpost(Post newBlogpost)
    {
        jdbcTemplate.update("insert into posts (" +
                        "`postMainTitle`, " +
                        "`postSubTitle`, " +
                        "`postLead`, " +
                        "`postBodytext`, " +
                        "`postImage`) " +
                        "values (?, ?, ?, ?, ?)",
                        newBlogpost.getPostMainTitle(),
                        newBlogpost.getPostSubTitle(),
                        newBlogpost.getPostLead(),
                        newBlogpost.getPostBodytext(),
                        newBlogpost.getPostImage());
    }

    public void updateExistingBlogpost(Post existingBlogpost)
    {
        jdbcTemplate.update("update posts set " +
                        "`postMainTitle` = ?, " +
                        "`postSubTitle` = ?, " +
                        "`postLead` = ?, " +
                        "`postBodytext` = ?, " +
                        "`postImage` = ? " +
                        "where `id` = ?",
                        existingBlogpost.getPostMainTitle(),
                        existingBlogpost.getPostSubTitle(),
                        existingBlogpost.getPostLead(),
                        existingBlogpost.getPostBodytext(),
                        existingBlogpost.getPostImage(),
                        existingBlogpost.getId()
        );
    }

    public void deleteBlogpost(int blogpostId)
    {
        jdbcTemplate.update("delete from posts where `id` = ?", blogpostId);
    }
}
