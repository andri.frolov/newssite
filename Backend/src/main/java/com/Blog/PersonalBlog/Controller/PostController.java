package com.Blog.PersonalBlog.Controller;

import com.Blog.PersonalBlog.Model.Post;
import com.Blog.PersonalBlog.Repository.PostRepository;
import com.Blog.PersonalBlog.Service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/posts")
@CrossOrigin
public class PostController
{
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private BlogService blogService;

    @GetMapping("/getPosts")
    public List<Post> getAllPosts()
    {
        System.out.println("Getting all posts...");
        return postRepository.fetchAllPosts();
    }

    @GetMapping("/getPosts/{id}")
    public Post getSinglePostById(@PathVariable int id)
    {
        System.out.println("Getting single post...");
        return postRepository.fetchSinglePostById(id);
    }

    @RequestMapping(value = "/editPost",
            method = {RequestMethod.POST, RequestMethod.PUT})
    public void editPost(@RequestBody Post blogpost)
    {
        System.out.println("Editing single post or adding a new one...");
        blogService.editPost(blogpost);
    }

    @DeleteMapping("/deletePost/{blogpostId}")
    public void deletePost(@PathVariable int blogpostId)
    {
        System.out.println("Deleting blogpost @ id " + blogpostId + ".");
        postRepository.deleteBlogpost(blogpostId);
    }
}
