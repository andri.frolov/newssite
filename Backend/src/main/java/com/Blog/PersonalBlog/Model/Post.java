package com.Blog.PersonalBlog.Model;

public class Post
{
    private int id;
    private String postMainTitle;
    private String postSubTitle;
    private String postLead;
    private String postBodytext;
    private String postImage;

    public Post(int id, String postMainTitle, String postSubTitle, String postLead, String postBodytext, String postImage)
    {
        this.id = id;
        this.postMainTitle = postMainTitle;
        this.postSubTitle = postSubTitle;
        this.postLead = postLead;
        this.postBodytext = postBodytext;
        this.postImage = postImage;
    }

    public int getId()
    {
        return id;
    }

    public String getPostMainTitle()
    {
        return postMainTitle;
    }

    public String getPostSubTitle()
    {
        return postSubTitle;
    }

    public String getPostBodytext()
    {
        return postBodytext;
    }

    public String getPostImage()
    {
        return postImage;
    }

    public String getPostLead()
    {
        return postLead;
    }
}
